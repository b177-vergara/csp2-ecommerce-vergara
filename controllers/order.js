const order = require("../models/order");
const product = require("../models/product");
const user = require("../models/user");

// Checkout order
module.exports.order = (data) => {
	// check if admin
	if(!data.isAdmin){
		// check if product id exists
		return product.find({_id : data.productId}).then(result => {
			// if no registered product id
			if(result.length == 0){
				return (`Sorry. No registered product for Product ID # ${data.productId}.`);
			}
			else{
				// compute total price
				let total = data.quantity * result[0].price;

				const newOrder = new order({
					userId : data.userId,
					productId : data.productId,
					quantity : data.quantity,
					totalAmount : total
				});

				// if product quantity < input quantity
				if((result[0].quantity < data.quantity) || data.quantity == 0){
					return (`Insufficient stock or invalid quantity.`);
				}
				else{
					return newOrder.save().then( async (order, err) => {
						if(err){
							return err;
						}
						else{
							
							let isUserUpdated = await user.findById(data.userId).then(user => {
								user.orders.push({
									productId : data.productId,
									quantity : order.quantity,
									totalAmount : order.totalAmount
								});
								return user.save().then((user, err) => {
									if(err){
										return err;
									}
									else{
										return true;
									}
								})
							})
						
							let isProductUpdated = await product.findById(data.productId).then(product => {
								product.orders.push({
									userId : data.userId,
									quantity : data.quantity
								});
								return product.save().then((product, err) => {
									if(err){
										return err;
									}
									else{
										return true;
									}
								})
							})

							let isProductQtyUpdated = product.findById(data.productId).then(productQty => {

                                let newQuantity =  productQty.quantity - data.quantity;

                                if(newQuantity == 0){

                                    const newProduct = {
                                        quantity : newQuantity,
                                        isActive : false
                                    };

                                    return product.findByIdAndUpdate(data.productId, newProduct).then((Product, err) => {
                                        if(err){
                                            return err;
                                        }
                                        else{
                                            return true;
                                        }
                                    })
								}
                                else {
                                    const newProduct = {
                                        quantity : newQuantity
                                    };

                                    return product.findByIdAndUpdate(data.productId, newProduct).then((Product, err) => {
                                        if(err){
                                            return err;
                                        }
                                        else{
                                            return true;
                                        }
                                    })
                                }
                            })

							if(isUserUpdated && isProductUpdated && isProductQtyUpdated){
								return (`${data.firstName} ${data.lastName}, you have successfully ordered ${order.quantity} pc/s of Product ID # ${order.productId} with a total price of ₱${order.totalAmount}.`);
							}
						
							else{
								return false;
							}
						}
					});
				}}
		})
	}
	else{
		return Promise.resolve(`Access denied. ${data.firstName} ${data.lastName} is an admin.`);
	}
}

// Get all orders (Admin only)
module.exports.getAllOrders = (userData) => {
	if(userData.isAdmin){
		return order.find({}).then(result =>{
			// if no registered order
			if(result.length == 0){
				return (`No registered order at the moment.`);
			}
			else{
				return result;
			}
		})
	}
	else{
		return Promise.resolve(`${userData.email} is not an admin.`);
	}
}

// Get all orders for user
module.exports.getOrders = (userData) => {
	return order.find({userId : userData.userId}).then(result =>{
		// if no registered order
		if(result.length == 0){
			return (`No registered order at the moment for ${userData.email}.`);
		}
		else{
			return result;
		}
	})
}