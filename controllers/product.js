const product = require("../models/product");

// Register a product
module.exports.createProduct = (reqBody, userData) => {
	if(userData.isAdmin){
		return product.findOne({name : reqBody.name.toLowerCase()}).then(result => {
			// if product name is available
			if(!result){
				let newProduct = new product({
					name : reqBody.name.toLowerCase(),
					description : reqBody.description,
					price : reqBody.price,
					quantity : reqBody.quantity
				});
				return newProduct.save().then((product, err) => {
					if(err){
						return false;
					}
					else{
						return (`Registration for ${reqBody.name} is successful.`);
					}
				});
			}
			else{
				return (`${reqBody.name} is already registered.`);
			}
		});
	}
	else{
		return Promise.resolve(`Access denied. ${userData.email} is not an admin.`);
	}
}

// Get all Products
module.exports.getAllProducts = (userData) => {
	if(userData.isAdmin){
		return product.find({}).then(result =>{
			// if no registered product
			if(result.length == 0){
				return (`No registered product at the moment.`);
			}
			else{
				return result;
			}
		})
	}
	else{
		return Promise.resolve(`Access denied. ${userData.email} is not an admin.`);
	}
}

// Get all Active Products
module.exports.getAllActiveProducts = () => {
	return product.find({isActive : true}).then(result =>{
		// if no registered product
		if(result.length == 0){
			return (`No registered product at the moment.`);
		}
		else{
			return result;
		}
	})
}

// Get a specific Product
module.exports.getProduct = (reqParams) => {
	return product.findById(reqParams.productId).then(result =>{
		// if id is not found
		if(!result){
			return (`Product ID number: ${reqParams.productId} is not registered.`);
		}
		else{
			return result;
		}
	})
}

// Updating a product
module.exports.updateProduct = (reqParams, data) => {
	if(data.isAdmin){
		let updatedProduct = {
			name : data.product.name.toLowerCase(),
			description : data.product.description,
			price : data.product.price,
			quantity : data.product.quantity,
			isActive : data.product.isActive
		}
		return product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if(error){
				return false;
			}
			else{
				return (`Successfully updated ${data.product.name}.`);
			}
		})
	}
	else{
		return Promise.resolve(`Access denied. ${data.email} is not an admin.`);
	}
}

// Archive a product
module.exports.archiveProduct = (reqParams, data) => {
	if(data.isAdmin){
		let updatedProduct = {
			isActive : false
		}
		return product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if(error){
				return false;
			}
			else{
				return (`Successfully archived ${product.name}.`);
			}
		})
	}
	else{
		return Promise.resolve(`Access denied. ${data.email} is not an admin.`);
	}
}