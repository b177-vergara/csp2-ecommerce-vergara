const user = require("../models/user");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Register an account
module.exports.registerUser = (reqBody) => {
	return user.findOne({email : reqBody.email.toLowerCase()}).then(result => {
		// if email is available
		if(!result){
			let newUser = new user({
				email : reqBody.email.toLowerCase(),
				firstName : reqBody.firstName,
				lastName : reqBody.lastName,
				password : bcrypt.hashSync(reqBody.password, 10)
			});
			return newUser.save().then((user, err) => {
				if(err){
					return err;
				}
				else{
					return (`Registration for ${reqBody.email.toLowerCase()} is successful. Welcome ${reqBody.firstName} ${reqBody.lastName}!`);
				}
			});
		}
		else{
			return (`${reqBody.email.toLowerCase()} is already registered. Please try another Email address.`);
		}
	});
}

// User login
module.exports.loginUser = (reqBody) => {
	return user.findOne({email : reqBody.email.toLowerCase()}).then(result => {
		// if email is not registered
		if(!result){
			return (`${reqBody.email.toLowerCase()} is not registered.`);
		} 
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){
				return {Access : "Granted", Token : auth.createAccessToken(result)}
			}
			else{
				return (`Incorrect Password.`);
			}
		}
	});
}

// Set user as Admin
module.exports.setAsAdmin = (reqParams, userData) => {
	if(userData.isAdmin){
		return user.findById(reqParams.userId.toLowerCase()).then(result => {
			// if user id is already an admin
			if(result.isAdmin == true){
				return (`${result.firstName} ${result.lastName} is already an admin.`);
			}
			else{
				let updatedUser = {
					isAdmin : true
				}
				// update isAdmin value
				return user.findByIdAndUpdate(reqParams.userId.toLowerCase(), updatedUser).then((user, err) => {
					if(err){
						return false;
					}
					else{
						return (`${result.firstName} ${result.lastName} is now set to admin.`);
					}
				});
			}
		})
	}
	else{
		return Promise.resolve(`Access denied. ${userData.firstName.toLowerCase()} ${userData.lastName.toLowerCase()} is not an admin.`);
	}
}