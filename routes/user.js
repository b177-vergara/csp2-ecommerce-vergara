const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");

// Register an account
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// User login
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Set user as Admin
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {
    const userData = {
        email : auth.decode(req.headers.authorization).email,
		firstName : auth.decode(req.headers.authorization).firstName,
		lastName : auth.decode(req.headers.authorization).lastName,
		isAdmin : auth.decode(req.headers.authorization).isAdmin,
	}
    userController.setAsAdmin(req.params, userData).then(resultFromController => res.send(resultFromController));
});

module.exports = router;