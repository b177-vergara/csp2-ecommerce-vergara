const mongoose = require("mongoose");
const order = require("./order");

const productSchema = new mongoose.Schema({
    name : {
        type : String,
        required : [true, "Product name is required."]
    },
    description : {
        type : String,
        required : [true, "Product description is Required."]
    },
    price : {
        type : Number,
        ref : 'order',
        required : [true, "Price is Required."]
    },
    quantity : {
        type : Number,
        required : [true, "Quantity is Required."]
    },
    isActive : {
        type : Boolean,
        default : true
    },
    createdOn : {
        type : Date,
        default : Date.now
    },
    orders : [
        {
            userId : {
                type : String,
            },
            quantity : {
                type : Number
            }
        }
    ]
});

module.exports = mongoose.model("product", productSchema);