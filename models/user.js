const mongoose = require("mongoose");


const userSchema = new mongoose.Schema({
    email : {
        type : String,
        required : [true, "Email is Required."]
    },
    firstName : {
        type : String,
        required : [true, "First name is Required."]
    },
    lastName : {
        type : String,
        required : [true, "Last name is Required."]
    },
    password : {
        type : String,
        required : [true, "Password is Required."]
    },
    isAdmin : {
        type : Boolean,
        default : false
    },
    orders : [{
        productId : {
            type : String
        },
        quantity : {
            type : String
        },
        totalAmount : {
            type : String
        },
    }]
});

module.exports = mongoose.model("user", userSchema);