const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
    productId : {
        type : String,
        required : [true, "Product ID Required."]
    },
    userId : {
        type : String
    },
    quantity : {
        type : Number,
        required : [true, "Quantity Required."]
    },
    totalAmount : {
        type : Number
    },
    purchasedOn : {
        type : Date,
        default : Date.now
    }
});

module.exports = mongoose.model("order", orderSchema);